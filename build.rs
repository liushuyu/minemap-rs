use std::{
    env,
    fs::{self, File},
    io::Write,
    path::Path,
};

use nom::{
    branch::alt,
    bytes::complete::{is_not, tag},
    character::{
        complete::{char, line_ending, multispace0, not_line_ending},
        streaming::digit1,
    },
    combinator::{map, value},
    error::{make_error, ErrorKind, ParseError},
    multi::{fold_many1, separated_list1},
    sequence::{delimited, pair, terminated},
    Err::Error,
    IResult, Parser,
};

const PALETTES: &[&str] = &["1.8", "1.12", "1.16"];

fn one_line(input: &str) -> IResult<&str, Vec<u8>> {
    alt((
        parsed_array,
        map(not_line_ending, |_| Vec::new()),
        map(comment, |_| Vec::new()),
    ))(input)
}

fn ws<'a, O, E: ParseError<&'a str>, F: Parser<&'a str, O, E>>(f: F) -> impl Parser<&'a str, O, E> {
    delimited(multispace0, f, multispace0)
}

fn parsed_array(input: &str) -> IResult<&str, Vec<u8>> {
    let list = array(input)?;
    let mut result = vec![];
    if list.1.len() != 4 {
        return Err(Error(make_error(input, ErrorKind::Verify)));
    }
    for i in list.1 {
        result.push(
            i.parse::<u8>()
                .map_err(|_| Error(make_error(input, ErrorKind::Verify)))?,
        );
    }

    Ok((list.0, result))
}

fn array(input: &str) -> IResult<&str, Vec<&str>> {
    delimited(
        char('{'),
        ws(separated_list1(ws(char(',')), digit1)),
        tag("},"),
    )(input)
}

fn comment<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, (), E> {
    value((), pair(char('#'), is_not("\n\r")))(i)
}

fn parse_palette(input: &str) -> IResult<&str, Vec<u8>> {
    fold_many1(
        terminated(one_line, line_ending),
        Vec::new(),
        |mut acc: Vec<_>, item| {
            acc.extend(item);
            acc
        },
    )(input)
}

fn main() {
    let mut output =
        File::create(Path::new(&env::var("OUT_DIR").unwrap()).join("palettes.rs")).unwrap();
    for v in PALETTES {
        let text = fs::read_to_string(Path::new("assets").join(format!("rgba-{}.txt", v))).unwrap();
        let data = parse_palette(&text).unwrap();
        if data.0.len() > 0 {
            panic!("Parse failed, {} characters remaining", data.0.len());
        }
        output
            .write_all(
                format!(
                    "const PALETTE_{}: &[u8] = &{:?};\n",
                    v.replace(".", "_"),
                    data.1
                )
                .as_bytes(),
            )
            .unwrap();
    }
}

#[test]
fn test_array() {
    assert_eq!(one_line("{0,0,0,0},"), Ok(("", vec![0, 0, 0, 0])));
    assert_eq!(one_line("#comment"), Ok(("", vec![])));
    assert_eq!(
        parse_palette("{0,0,0,0},\n{1,2,3,4},\n"),
        Ok(("", vec![0, 0, 0, 0, 1, 2, 3, 4]))
    );
}
