use anyhow::Result;

use color_quant::NeuQuant;
use image::{
    imageops::{dither, index_colors},
    ImageBuffer, Luma, Rgba, DynamicImage
};

use crate::mc_ver::GameVer;

include!(concat!(env!("OUT_DIR"), "/palettes.rs"));

#[inline]
fn write_preview(image: &ImageBuffer<Rgba<u8>, Vec<u8>>, path: &str) -> Result<()> {
    DynamicImage::ImageRgba8(image.to_owned()).save(path)?;

    Ok(())
}

pub fn make_color_map(game_ver: &GameVer) -> NeuQuant {
    let palette = match game_ver {
        GameVer::None => PALETTE_1_8,
        GameVer::Mc1_8 => PALETTE_1_8,
        GameVer::Mc1_12 => PALETTE_1_12,
        GameVer::Mc1_16 => PALETTE_1_16,
    };

    NeuQuant::new(15, palette.len(), palette)
}

pub fn map_to_color_code(
    image: &ImageBuffer<Rgba<u8>, Vec<u8>>,
    game_ver: &GameVer,
    export: Option<&str>,
    dithering: bool,
) -> ImageBuffer<Luma<u8>, Vec<u8>> {
    let color_map = make_color_map(game_ver);
    if !dithering {
        if let Some(path) = export {
            write_preview(image, path).ok();
        }
        return index_colors(image, &color_map);
    }
    let mut dither_image = image.clone();
    dither(&mut dither_image, &color_map);
    if let Some(path) = export {
        write_preview(image, path).ok();
    }

    index_colors(&dither_image, &color_map)
}

pub fn convert_to_mc_format(image: ImageBuffer<Luma<u8>, Vec<u8>>) -> Vec<i8> {
    let data = image.into_raw();
    let mut data = std::mem::ManuallyDrop::new(data);
    let pointer = data.as_mut_ptr();
    let len = data.len();
    let cap = data.capacity();

    unsafe { Vec::from_raw_parts(pointer as *mut i8, len, cap) }
}
