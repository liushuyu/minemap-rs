use anyhow::Result;
use nbt::{Blob, Map, Value};
use std::{fs::File, path::Path};

use crate::mc_ver::GameVer;

fn insert_version_fields(data: &mut Map<String, Value>, game_ver: &GameVer) {
    data.insert("width".to_string(), Value::Short(128));
    data.insert("height".to_string(), Value::Short(128));
    match game_ver {
        GameVer::None => {}
        GameVer::Mc1_8 => {}
        GameVer::Mc1_12 | GameVer::Mc1_16 => {
            data.insert("trackingPosition".to_string(), Value::Byte(0));
            data.insert("unlimitedTracking".to_string(), Value::Byte(0));
            data.insert("locked".to_string(), Value::Byte(1));
            data.insert("banners".to_string(), Value::List(vec![]));
            data.insert("frames".to_string(), Value::List(vec![]));
        }
    }
}

pub fn make_nbt_blob(game_ver: &GameVer, image: Vec<i8>) -> Blob {
    let mut data = Map::new();
    let mut map = Blob::new();
    // hardcoded-fields
    data.insert("scale".to_string(), Value::Short(1));
    data.insert("dimension".to_string(), Value::Byte(1));
    data.insert("xCenter".to_string(), Value::Int(1 << 30));
    data.insert("zCenter".to_string(), Value::Int(1 << 30));
    data.insert("colors".to_string(), Value::ByteArray(image));
    insert_version_fields(&mut data, game_ver);

    // create tag
    map.insert("data", Value::Compound(data)).unwrap();

    map
}

pub fn write_nbt_blob<P: AsRef<Path>>(dst: P, blob: Blob, gzip: bool) -> Result<()> {
    let mut f = File::create(dst)?;
    if gzip {
        blob.to_gzip_writer(&mut f)?;
    } else {
        blob.to_writer(&mut f)?;
    }

    Ok(())
}
