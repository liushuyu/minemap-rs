mod cli;
mod colormap;
mod map;
mod mc_ver;

use anyhow::Result;
use image::{imageops, io::Reader as ImageBuffer};

fn main() -> Result<()> {
    let args = cli::build_cli().get_matches();
    let game_ver = mc_ver::GameVer::from(args.value_of("game").unwrap());
    let mut gzip = true;
    if game_ver == mc_ver::GameVer::None {
        panic!("Invalid game version!");
    }
    let input = args.value_of("input").unwrap();
    let output = args.value_of("output").unwrap();
    let export = args.value_of("export");
    if args.is_present("no-gz") {
        gzip = false;
    }

    let image = ImageBuffer::open(input)?
        .decode()?
        .resize(128, 128, imageops::FilterType::Nearest);
    let map_code = colormap::map_to_color_code(
        &image.to_rgba8(),
        &game_ver,
        export,
        args.is_present("dithering"),
    );
    let mc_map_code = colormap::convert_to_mc_format(map_code);
    let nbt_blob = map::make_nbt_blob(&game_ver, mc_map_code);
    map::write_nbt_blob(output, nbt_blob, gzip)?;

    Ok(())
}
