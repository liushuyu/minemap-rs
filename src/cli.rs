use clap::{App, Arg};

use crate::mc_ver::SUPPORTED_VERSIONS;

pub fn build_cli() -> App<'static, 'static> {
    App::new("minemap")
        .about("Convert images to Minecraft map items")
        .arg(
            Arg::with_name("game")
                .short("g")
                .long("game")
                .required(true)
                .takes_value(true)
                .possible_values(SUPPORTED_VERSIONS)
                .help("Minecraft game version this map is going to be used in"),
        )
        .arg(
            Arg::with_name("export")
                .short("e")
                .long("export")
                .required(false)
                .takes_value(true)
                .help("Export the result of color reduction in png format"),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .required(true)
                .takes_value(true)
                .help("Output file in NBT format"),
        )
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .required(true)
                .takes_value(true)
                .help("Input image"),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .takes_value(false)
                .help("Turn on verbose output"),
        )
        .arg(
            Arg::with_name("no-gz")
                .long("no-gz")
                .takes_value(false)
                .help("Do not compress NBT file using gzip"),
        )
        .arg(
            Arg::with_name("dithering")
                .short("-d")
                .long("dithering")
                .takes_value(true)
                .required(false)
                .possible_value("floydsteinberg")
                .help("Turns on dithering"),
        )
}
