use std::fmt::Display;

pub const SUPPORTED_VERSIONS: &[&str] = &["1.8", "1.12", "1.16"];

#[derive(Debug, PartialEq)]
pub enum GameVer {
    None,
    Mc1_8,
    Mc1_12,
    Mc1_16,
}

impl Display for GameVer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GameVer::None => f.write_str("?"),
            GameVer::Mc1_8 => f.write_str("1.8"),
            GameVer::Mc1_12 => f.write_str("1.12"),
            GameVer::Mc1_16 => f.write_str("1.16"),
        }
    }
}

impl From<&str> for GameVer {
    fn from(input: &str) -> Self {
        match input {
            "1.8" => GameVer::Mc1_8,
            "1.12" => GameVer::Mc1_12,
            "1.16" => GameVer::Mc1_16,
            _ => GameVer::None,
        }
    }
}
